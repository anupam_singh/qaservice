package com.stackOverFlow;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.servicediscovery.ServiceDiscovery;

public class CloudQAService extends DefaultQAService {

	private static final String USER_SERVICE_HOST = "104.154.248.9";
	
	@Override
	protected String getMongoDBHost() {
		return "104.197.175.52";
	}

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
	}

	@Override
	protected void getHttpClient(Handler<AsyncResult<Object>> handler) {
		handler.handle(Future.succeededFuture(vertx.createHttpClient()));
	}

	@Override
	protected void doAuthorization(RoutingContext context, HttpClient client, String path,
			Handler<AsyncResult<Object>> handler) {
		// run with circuit breaker in order to deal with failure
				circuitBreaker.execute(future -> {
					HttpClientRequest toReq = client.request(context.request().method(),80, USER_SERVICE_HOST, path, response -> {
						response.bodyHandler(body -> {
							System.out
									.println("CloudQAService.doAuthorization() response.statusCode(): " + response.statusCode());
							if (response.statusCode() >= 500) { // api endpoint server
																// error, circuit
																// breaker
																// should fail
								future.fail(response.statusCode() + ": " + body.toString());
								handler.handle(Future.failedFuture(response.statusCode() + ": " + body.toString()));
							} else {
								System.out.println("CloudQAService.doAuthorization() body: " + body);
								JsonObject userObj = body.toJsonObject();
								System.out.println("CloudQAService.doAuthorization()" + userObj.getString("username"));
//								handler.handle(Future
//										.succeededFuture(response.statusCode() + ":" + usernameObj.getString("username")));
								handler.handle(Future
										.succeededFuture(getUserDetails(response.statusCode(),userObj)));
								future.complete();
							}
							ServiceDiscovery.releaseServiceObject(discovery, client);
						});
					});
					// set headers
					context.request().headers().forEach(header -> {
						toReq.putHeader(header.getKey(), header.getValue());
					});
					if (context.user() != null) {
						toReq.putHeader("user-principal", context.user().principal().encode());
					}
					System.out.println("CloudQAService.doAuthorization() context.getBody(): " + context.getBody());
					// send request
					if (context.getBody() == null) {
						toReq.end();
					} else {
						toReq.end(context.getBody());
					}
				}).setHandler(ar -> {
					if (ar.failed()) {
						badGateway(ar.cause(), context);
						handler.handle(Future.failedFuture("bad_gateway"));
					}
				});
	}

	@Override
	protected void createHttpServer(Router router, Future<Void> future) {
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);
	}

}
